#include "colorlampwgt.h"
#include "colorlampwgtplugin.h"

#include <QtPlugin>

ColorLampWgtPlugin::ColorLampWgtPlugin(QObject *parent)
    : QObject(parent)
{
}

void ColorLampWgtPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool ColorLampWgtPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *ColorLampWgtPlugin::createWidget(QWidget *parent)
{
    return new ColorLampWgt(Qt::gray, parent);
}

QString ColorLampWgtPlugin::name() const
{
    return QLatin1String("ColorLampWgt");
}

QString ColorLampWgtPlugin::group() const
{
    return QLatin1String("Custom Widgets");
}

QIcon ColorLampWgtPlugin::icon() const
{
    return QIcon(QLatin1String(":/lamp.png"));
}

QString ColorLampWgtPlugin::toolTip() const
{
    return QLatin1String("This is color status lamp");
}

QString ColorLampWgtPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool ColorLampWgtPlugin::isContainer() const
{
    return false;
}

QString ColorLampWgtPlugin::domXml() const
{
    return QLatin1String(R"(<widget class="ColorLampWgt" name="colorLampWgt">
</widget>)");
}

QString ColorLampWgtPlugin::includeFile() const
{
    return QLatin1String("colorlampwgt.h");
}
