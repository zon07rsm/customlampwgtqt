#include "colorlampwgt.h"

ColorLampWgt::ColorLampWgt(Qt::GlobalColor lampColor_a, QWidget *parent)
    : QFrame(parent)
{
    this->setGeometry(0,0,50,50);
    this->lampColor_m = lampColor_a;
    QPalette plt(lampColor_m);
    this->setPalette(plt);
    this->setFrameShape(QFrame::Box);
    this->setFrameShadow(QFrame::Raised);
    this->setAutoFillBackground(true);

}

Qt::GlobalColor ColorLampWgt::getLampColor()
{
    return this->lampColor_m;
}

void ColorLampWgt::setLampColor(Qt::GlobalColor lampColor)
{

    QPalette plt(lampColor);
    this->setPalette(plt);
    emit this->lampColorChanged(lampColor);
}
