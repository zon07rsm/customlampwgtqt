#ifndef COLORLAMPWGT_H
#define COLORLAMPWGT_H

#include <QFrame>

class ColorLampWgt : public QFrame
{
    Q_OBJECT
public:
    explicit ColorLampWgt(Qt::GlobalColor lampColor_a = Qt::gray, QWidget *parent = nullptr);

    Qt::GlobalColor getLampColor();
    void setLampColor(Qt::GlobalColor lampColor);

signals:
    void lampColorChanged(Qt::GlobalColor newColor);

private:
    Qt::GlobalColor lampColor_m;
};

#endif // COLORLAMPWGT_H
