CONFIG      += plugin debug_and_release
TARGET      = $$qtLibraryTarget(colorlampwgtplugin)
TEMPLATE    = lib

HEADERS     = colorlampwgtplugin.h
SOURCES     = colorlampwgtplugin.cpp
RESOURCES   = icons.qrc
LIBS        += -L. 

QT += designer

target.path = $$OUT_PWD/designer
INSTALLS    += target

include(colorlampwgt.pri)
